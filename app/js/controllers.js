'use strict';

/* Controllers */

angular.module('cpe-calendar.controllers', []).

	controller('IntroController', function($scope) {

	})
	.controller('Step1Controller', function($scope) {

	}).
	controller('Step2Controller', function($scope) {

  }).
	controller('Step3Controller', function($scope, $http, $filter, sharedProperties) {
		$scope.events = sharedProperties.getForm();
		$scope.eForms = {};
		$scope.eForms.events = {};
		$scope.eForms.company = false;
		$scope.eForms.companyMorning = "08:00 - 12:00";
		$scope.eForms.companyAfternoon = "14:00 - 18:00";
		$scope.eForms.groupTF = false;
		$scope.eForms.group = "Groupe 1";

		$scope.next = function()
		{
			sharedProperties.setForm($scope.eForms);
		};

		$http(
			{
				method: 'GET',
				url: '/data/analysis.php'
			}).
			success(function(data, status, headers, config)
			{
				$scope.events = data;
				for (var i = 0; i < $scope.events.length; i++) {
					$scope.eForms.events[$scope.events[i]] = true;
				};
			}).
			error(function(data, status, headers, config)
			{
				$scope.events = ['Erreur ! '];
			});
  }).
	controller('Step4Controller', function($scope, $http, sharedProperties) {

		var eventsList = [];
		for (var i in sharedProperties.getForm().events)
		{
			if(sharedProperties.getForm().events[i])
				eventsList.push(i);
		}

		$http(
			{
				method: 'GET', 
				url: '/data/fill.php', 
				params: 
				{
					company: sharedProperties.getForm().company, 
					companyMorning: sharedProperties.getForm().companyMorning, 
					companyAfternoon: sharedProperties.getForm().companyAfternoon, 
					groupTF: sharedProperties.getForm().groupTF, 
					group: sharedProperties.getForm().group, 
					eventsList: eventsList
				}
			}).
			success(function(data, status, headers, config)
			{
				console.log("Done.");
			}).
			error(function(data, status, headers, config)
			{
				$scope.events = ['Erreur ! '];
			});

  });
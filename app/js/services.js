'use strict';

/* Services */


angular.module('cpe-calendar.services', [])
    .service('sharedProperties', function () {
        var form = null;
        var company = null;
        var companyMorning = null;
        var companyAfternoon = null;

        return {
            getForm: function () {
                return form;
            },
            setForm: function(value) {
                form = value;
            },
            getCompany: function () {
                return company;
            },
            setCompany: function(value) {
                company = value;
            },
            getCompanyAfternoon: function () {
                return companyAfternoon;
            },
            setCompanyAfternoon: function(value) {
                companyAfternoon = value;
            },
            getCompanyMorning: function () {
                return companyMorning;
            },
            setCompanyMorning: function(value) {
                companyMorning = value;
            }
        };
    });
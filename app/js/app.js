'use strict';

angular.module('cpe-calendar', ['cpe-calendar.filters', 'cpe-calendar.services', 'cpe-calendar.directives', 'cpe-calendar.controllers']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'partials/intro.php', controller: 'IntroController'});
    $routeProvider.when('/step-1', {templateUrl: 'partials/step-1.php', controller: 'Step1Controller'});
    $routeProvider.when('/step-2', {templateUrl: 'partials/step-2.php', controller: 'Step2Controller'});
    $routeProvider.when('/step-3', {templateUrl: 'partials/step-3.php', controller: 'Step3Controller'});
    $routeProvider.when('/step-4', {templateUrl: 'partials/step-4.php', controller: 'Step4Controller'});
    $routeProvider.otherwise({redirectTo: '/'});
  }]);


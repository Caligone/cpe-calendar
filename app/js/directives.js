'use strict';

/* Directives */


angular.module('cpe-calendar.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);

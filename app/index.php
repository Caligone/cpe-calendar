<?php
	session_start();
	require_once 'lib/GoogleAPI/Google_Client.php';
	require_once 'lib/GoogleAPI/contrib/Google_CalendarService.php';

	$client = new Google_Client();
	$client->setApplicationName("Google Calendar PHP Starter Application");
	$client->setApprovalPrompt('auto');
	$client->setAccessType("offline");

	$client->setClientId('895906685736.apps.googleusercontent.com');
	$client->setClientSecret('l7sYXpme1KL5JscfAfXbvSzf');
	$client->setRedirectUri('http://cpe.caligone.fr');

	$cal = new Google_CalendarService($client);

	// Logout
	if (isset($_GET['logout']))
		unset($_SESSION['token']);

	// Asking a login
	if (isset($_GET['code']))
	{
		$client->authenticate($_GET['code']);
		$_SESSION['token'] = $client->getAccessToken();
		header('Location: http://cpe.caligone.fr/#/step-3');
	}

	// Get back a login
	if (isset($_SESSION['token']))
	{
		$client->setAccessToken($_SESSION['token']);
	}

	// Logged
	if ($client->getAccessToken())
	{
		$_SESSION['token'] = $client->getAccessToken();
		$_SESSION['authUrl'] = null;
	}
	// Not logged
	else
	{
		$_SESSION['authUrl'] = $client->createAuthUrl();
	}
?>
<!doctype html>
<html lang="fr" ng-app="cpe-calendar">
<head>

	<title>CPE Calendar</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSS -->
	<link rel="stylesheet" href="css/app.css"/>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>

</head>
<body>

	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<div class="page-header text-center">
				<h1>CPE Calendar <small>Alpha</small></h1>
			</div>
			<div class="clearfix"></div>
			<div ng-view></div>
		</div>
	</div>

	<!-- JS -->
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="lib/angular/angular.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/app.js"></script>
	<script src="js/services.js"></script>
	<script src="js/controllers.js"></script>
	<script src="js/filters.js"></script>
	<script src="js/directives.js"></script>

</body>
</html>

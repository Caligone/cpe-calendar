<?php session_start(); ?>
<h4>Connexion à Google</h4>
<div>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, obcaecati nihil fugit eius beatae recusandae suscipit accusantium consectetur provident incidunt at repudiandae qui placeat perferendis enim! Alias ipsam eum necessitatibus.</p>

	<div class="text-center">
		<?php 
		if($_SESSION['authUrl'] == null)
		{
			echo '<a href="#/step-3" type="button" class="btn btn-primary pull-right">Suivant</a>';
		}
		else
		{
			echo '<a href="'.$_SESSION['authUrl'].'" type="button" class="btn btn-primary pull-right">Se connecter</a>';
		}
		?>
		<a href="#/step-1" type="button" class="btn btn-primary pull-left">Précédent</a>
	</div>
</div>

<?php session_start(); ?>
<h4>Analyse de l'emploi du temps annuel</h4>
<div>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, quam, nemo perspiciatis dolor neque ipsa distinctio voluptate magnam labore ipsam praesentium consectetur ratione officia recusandae voluptatum a accusamus error doloremque.</p>	
	<form novalidate class="form">
	  	<div class="checkbox">
	  		<blockquote ng-show="!events.length"><img src="img/loader.gif"> Le fichier est en cours d'analyse...</blockquote>

	  		<!-- Courses -->
	  		<div ng-show="events.length" class="panel panel-default">
				<div class="panel-heading">
			    	<h3 class="panel-title">Liste des cours</h3>
				</div>
				<div class="panel-body" style="padding-left:15%" >
					<ul class="list-unstyled">
					<li ng-repeat="event in events">
						<label>
				    		<input type="checkbox" ng-model="eForms.events[event]"> {{event}}
				  		</label>
					</li>
				</ul>
				</div>
			</div>

			<!-- Options -->
			<div ng-show="events.length" class="panel panel-default">
				<div class="panel-heading">
			    	<h3 class="panel-title">Autres options</h3>
				</div>
				<div class="panel-body" style="padding-left:15%" >
					<label>
				    	<input type="checkbox" ng-model="eForms.company" checked="checked"> <b>Afficher les périodes en entreprise</b>
				  	</label>
				  	<div class="clearfix"></div>
				  	<div class="col-lg-6" ng-show="eForms.company">
						<label>Matin</label>
				    	<input type="text" class="form-control" ng-model="eForms.companyMorning" placeholder="8:00 - 12:00">
				    </div>
				  	<div class="col-lg-6" ng-show="eForms.company">
						<label>Après-midi</label>
				    	<input type="text" class="form-control" ng-model="eForms.companyAfternoon" placeholder="13:00 - 17:00">
				  </div>
				  
				  	<div class="clearfix"></div>
				<label>
				    	<input type="checkbox" ng-model="eForms.groupTF" checked="checked"> <b>Afficher uniquement les cours de mon groupe</b>
				  	</label>
				  	<div class="clearfix"></div>
				  	<div class="col-lg-6" ng-show="eForms.groupTF">
						<label>
							<input type="radio" ng-model="eForms.group" value="Groupe 1"> Groupe 1
						</label>
						<label>
							<input type="radio" ng-model="eForms.group" value="Groupe 2"> Groupe 2
						</label>
				    </div>
				</div>
			</div>

		</div>
		<div class="text-center" ng-show="events.length" >
			<a href="#/step-4" type="button" ng-click="next()" class="btn btn-primary pull-right">Suivant</a>
			<a href="#/step-2" type="button" ng-click="previous()" class="btn btn-primary pull-left">Précédent</a>
		</div>
	</form>
</div>

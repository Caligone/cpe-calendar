<?php session_start(); ?>
<h4>Présentation</h4>
<p>Cette application permet de convertir le fichier <em>Excel</em> fourni par <em>CPE</em> en un calendrier interactif du service <em>Google Agenda</em>. Ainsi, vous pouvez accéder au calendrier de l'école directement sur votre smartphone ou sur n'importe quel ordinateur connecté.</p>
<div class="text-center">
	<a href="#/step-1" type="button" class="btn btn-primary btn-lg">Commencer</a>
</div>
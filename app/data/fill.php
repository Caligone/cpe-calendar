<?
	session_start();
	date_default_timezone_set("Europe/Paris");

	// Libraries
	require_once '../lib/PHPExcel/PHPExcel/IOFactory.php';
	require_once '../lib/PHPExcel/PHPExcel/IOFactory.php';
	require_once '../lib/GoogleAPI/Google_Client.php';
	require_once '../lib/GoogleAPI/contrib/Google_CalendarService.php';

	// @TODO Clean this lines
	// $_GET['eventsList'] = explode(',', preg_replace('["\[\]]', '', $_GET['eventsList']));
	$_GET['eventsList'] = str_replace('"', "", $_GET['eventsList']);
	$_GET['eventsList'] = str_replace('[', "", $_GET['eventsList']);
	$_GET['eventsList'] = str_replace(']', "", $_GET['eventsList']);
	$_GET['eventsList'] = explode(',', $_GET['eventsList']);

	// Test file
	if (!file_exists("../../files/annuel.xlsx"))
		exit("The file was not found.");
	
	// Load the file
	$reader = PHPExcel_IOFactory::createReader('Excel2007');
	$file = $reader->load('../../files/annuel.xlsx');
	$fileContent = $file->getActiveSheet()->toArray(null,true,true,true);

	// Read and fill the array
	$events = array();
	$currentDate = null;
	// Lines
	foreach ($fileContent as $key => $value)
	{
		// Columns
		foreach ($value as $key2 => $value2)
		{
			// Limits of the tab on the X axis
			if(strlen($key2) == 1 && $key2 > 'A')
			{
				// It's a date
				if(preg_match("/\d{1,2}\/\d{1,2}/", $value2))
				{
					$events[$value2] = array();
					$currentDate = $value2;
				}
				// It's an event
				elseif($currentDate != null && $value2 != "")
				{
					// Clean the event name and avoid duplication entry
					$value2 = preg_replace("/ [0-9]+/", "", $value2);
					if(in_array($value2, $_GET['eventsList']))
						$events[$currentDate][] = $value2;
					else
						echo "$value2 is not allowed<br/>";
				}
			}
		}
	}
	
	// Clean and reorganize the array
	foreach ($events as $key => $value)
	{
		// Empty day
		if($value == null)
		{
			// Add the company period
			if($_GET['company'] == "true")
				$events[$key] = "Entreprise";
			// Just delete the entry
			else
				unset($events[$key]);
		}
		// A course was found
		else
		{
			// Only one entry
			if (count($value) == 1)
			{
				unset($events[$key]);
				$events[$key] = $value[0];
			}
			// Two entries (Morning/Afternoon)
			elseif(count($value) == 2)
			{
				unset($events[$key]);
				$events[$key]['morning'] = $value[0];
				$events[$key]['afternoon'] = $value[1];
			}
			// Four entries (Morning/Afternoon for each group)
			elseif(count($value) >= 4)
			{
				unset($events[$key]);
				$events[$key]['Group1']['morning'] = $value[0];
				$events[$key]['Group1']['afternoon'] = $value[1];
				$events[$key]['Group2']['morning'] = $value[2];
				$events[$key]['Group2']['afternoon'] = $value[3];
			}
		}
	}

	// Configure Google Client
	$client = new Google_Client();
	$client->setApplicationName("Google Calendar PHP Starter Application");
	$client->setApprovalPrompt('auto');
	$client->setAccessType("offline");

	// Close your eyes
	$client->setClientId('895906685736.apps.googleusercontent.com');
	$client->setClientSecret('l7sYXpme1KL5JscfAfXbvSzf');
	$client->setRedirectUri('http://cpe.caligone.fr');
	// Okay, you can open your eyes
	

	$cal = new Google_CalendarService($client);

	$calList = $cal->calendarList->listCalendarList();

	echo "<pre>";
	print_r($_GET);
	print_r($events);
	echo "</pre>";
	
<?php
	session_start();
	date_default_timezone_set("Europe/Paris");

	// Libraries
	require_once '../lib/PHPExcel/PHPExcel/IOFactory.php';

	// Test file
	if (!file_exists("../../files/annuel.xlsx"))
	{
		exit("The file was not found.");
	}

	// Load the file
	$reader = PHPExcel_IOFactory::createReader('Excel2007');
	$file = $reader->load('../../files/annuel.xlsx');
	$fileContent = $file->getActiveSheet()->toArray(null,true,true,true);

	// Read and fill the array
	$eventsList = array();
	$currentDate = null;
	// Lines
	foreach ($fileContent as $key => $value)
	{
		// Columns
		foreach ($value as $key2 => $value2)
		{
			// Limits of the tab on the X axis
			if(strlen($key2) == 1 && $key2 > 'A')
			{
				// It's a date
				if(preg_match("/\d{1,2}\/\d{1,2}/", $value2))
					$currentDate = $value2;
				
				// It's an event
				elseif($currentDate != null && $value2 != "")
				{
					// Clean the event name and avoid duplication entry
					$value2 = preg_replace("/ [0-9]+/", "", $value2);
					if(!in_array($value2, $eventsList))
					{
						$eventsList[] = $value2;
					}
				}
			}
		}
	}
	sort($eventsList);
	echo json_encode($eventsList);
	